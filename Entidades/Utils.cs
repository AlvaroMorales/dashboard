﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Utils
    {
        public string formatCoin(double number)
        {
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            nfi.CurrencySymbol = "C$";
            return string.Format(nfi, "{0:C}", number); //?112.24
        }
        public string formatDecimal(decimal number)
        {
            return string.Format(CultureInfo.InvariantCulture, 
                                "{0:#0.##%}", number);
        }

    }
}
