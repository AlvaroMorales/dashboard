﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class RangoMora
    {
        public RangoMora() { }

        private string rangoMoraName;

        public string RangoMoraName
        {
            get { return rangoMoraName; }
            set { rangoMoraName = value; }
        }
        private double pendiente;

        public double Pendiente
        {
            get { return pendiente; }
            set { pendiente = value; }
        }
        private int clientes;

        public int Clientes
        {
            get { return clientes; }
            set { clientes = value; }
        }
        private double principal;

        public double Principal
        {
            get { return principal; }
            set { principal = value; }
        }



    }
}
