﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace Nexus
{
    public partial class TablaDetalle : System.Web.UI.Page
    {
        string rangoMora = "", group = "", periodicity = "", title = "Detalle de registros";
        DataTable dtBranches= new DataTable();
        DataTable dtOfficers = new DataTable();
        string branchesVar = "Managua";
        string officersVar = "";
        string date = "";
        
        ResumenGerencialNegocio resumenGerencial = new ResumenGerencialNegocio();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rangoMora = Request.Params["rangoMora"] != null ? Request.Params["rangoMora"].ToString() : "";
                group = Request.Params["group"] != null ? Request.Params["group"].ToString() : "";
                periodicity = Request.Params["periodicity"] != null ? Request.Params["periodicity"].ToString() : "";
                title = Request.Params["title"] != null ? Request.Params["title"].ToString() : "";
                branchesVar = Request.Params["branches"] == null ? ((string)Session["access"] == "Inmortal" ? resumenGerencial.getAllBranchesString() : (string)Session["branch"]) : Request.Params["branches"].ToString();
                date = Request.Params["date"] != null ? Request.Params["date"].ToString() : "";
                lblTitle.Text = title;
                officersVar = Request.Params["officers"] != null ? Request.Params["officers"].ToString() : "";

                //gvDetalle.DataSource = resumenGerencial.getDataByName(rangoMora, group, periodicity);
                //gvDetalle.DataBind();

                //lblTotalId.Text = gvDetalle.Rows.Count + "";
                ReportViewer2.Height = 1400;
                ReportViewer2.Width = 1200;

                ReportViewer2.ProcessingMode = ProcessingMode.Local;
                ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/Reportes/DashboardDataByName.rdlc");
                ReportViewer2.Visible = true;
                DataTable dt = resumenGerencial.getDataByName(branches(),officers(), rangoMora, group, periodicity, date);
                ReportDataSource rds = new ReportDataSource("DataByName_DS", dt);
                this.ReportViewer2.LocalReport.DataSources.Clear();
                ReportViewer2.LocalReport.DataSources.Add(rds);
            }
        }
        public DataTable branches()
        {   
            dtBranches.Columns.Add("StringItem", typeof(System.String));
            List<string> listBranches = new List<string>();
            string oneBranch = "";
            for (int i = 0; i < branchesVar.Length; i++)
            {
                if (!branchesVar.Substring(i,1).Equals("|") && i!=branchesVar.Length-1)
                {
                    oneBranch = oneBranch + branchesVar.Substring(i,1);
                }
                else
                {
                    oneBranch = branchesVar.Substring(i, 1)!="|"?oneBranch + branchesVar.Substring(i, 1):oneBranch+"";
                    dtBranches.Rows.Add(oneBranch);
                    oneBranch = "";
                }
            }
            //DataTable dt = new DataTable();
            //dt.Columns.Add("StringItem", typeof(System.String));
            //dt.Rows.Add("Chinandega");
            //return dt;
            foreach (DataRow row in dtBranches.Rows)
            {
               string name=  row[0].ToString();
            }
            return dtBranches;
        }
        public DataTable officers()
        {
            dtOfficers.Columns.Add("StringItem", typeof(System.String));
            List<string> listOfficers = new List<string>();
            string oneOfficer = "";
            for (int i = 0; i < officersVar.Length; i++)
            {
                if (!officersVar.Substring(i, 1).Equals("|") && i != officersVar.Length - 1)
                {
                    oneOfficer = oneOfficer + officersVar.Substring(i, 1);
                }
                else
                {
                    oneOfficer = officersVar.Substring(i, 1) != "|" ? oneOfficer + officersVar.Substring(i, 1) : oneOfficer + "";
                    dtOfficers.Rows.Add(oneOfficer);
                    oneOfficer = "";
                }
            }
            foreach (DataRow row in dtOfficers.Rows)
            {
                string name = row[0].ToString();
            }
            return dtOfficers;
        }

        //private void search(string text)
        //{
        //    bool exist = false;
        //    int count = 0;
        //    foreach (GridViewRow row in gvDetalle.Rows)
        //    {
        //        exist = false;
        //        for (int col = 0; col < gvDetalle.Rows[row.RowIndex].Cells.Count; col++)
        //        {
        //            if (!text.Equals(""))
        //            {
        //                if (gvDetalle.Rows[row.RowIndex].Cells[col].Text.ToString().ToUpper().Contains(text.ToUpper()))
        //                {
        //                    exist = true;
        //                    count++;
        //                    break;
        //                }
        //                else
        //                {
        //                    exist = false;
        //                }
        //            }
        //            else
        //            {
        //                exist= true;
        //                count++;
        //                break;
        //            }
        //        }
        //        row.Visible = exist;
        //    }

        //    lblTotalId.Text = count + "";
        //}

        //protected void txtSearchId_TextChanged(object sender, EventArgs e)
        //{
        //    search(txtSearchId.Text);
        //}


    }
}