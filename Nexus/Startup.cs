﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nexus.Startup))]
namespace Nexus
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
