﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TablaDetalle.aspx.cs" Inherits="Nexus.TablaDetalle" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <legend>
            <h2>
                <asp:Label Text="Detalle" ID="lblTitle" runat="server" Font-Bold="true" />
            </h2>
        </legend>
        <br />
        <div class="row">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <img src="Images/AroundBigger.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:GridView ID="gvDetalle" AutoGenerateColumns="true" CssClass="table" GridLines="None" runat="server">
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <rsweb:ReportViewer ID="ReportViewer2" runat="server" Font-Names="Verdana" Font-Size="10pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"></rsweb:ReportViewer>
        </div>
    </div>
</asp:Content>
