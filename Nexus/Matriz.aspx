﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Matriz.aspx.cs" Inherits="Nexus.Matriz" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="modalPopUpDistrucion" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-lg-12" style="width: 70%; align-content: center; text-align: center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label runat="server" Text="Distribución de Cartera" Font-Bold="true" Font-Size="X-Large" ID="Label1"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body modal-centertext">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView runat="server" ID="gvTable" DataKeyNames="Rango_Morosidad,Principal,Clientes,Pendiente" AutoGenerateColumns="false" CssClass="table" GridLines="None">
                                <Columns>
                                    <%--<asp:BoundField DataField="Rango_Morosidad" HeaderText="Rango de Morosidad" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Rango de Morosidad" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Rango_Morosidad" />
                                    <%--<asp:BoundField DataField="Principal" HeaderText="Principal" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Principal" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Principal" DataTextFormatString=" C${0:###,###,###.00}" />
                                    <%--<asp:BoundField DataField="Clientes" HeaderText="Clientes" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Pendiente" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Pendiente" DataTextFormatString=" C${0:###,###,###.00}" />
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Clientes" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Clientes" />
                                    <%--<asp:BoundField DataField="Pendiente" HeaderText="Pendiente" ShowHeader="True" Visible="True" />--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
