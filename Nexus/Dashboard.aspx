﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Nexus.Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />

    <%-- Gráfico 1 por saldos --%>
    <script type="text/javascript">

        google.charts.load('current', { 'packages': ['corechart'] });

        google.charts.load('current', { packages: ['corechart', 'bar'] });
        google.charts.setOnLoadCallback(drawStacked);

        google.charts.setOnLoadCallback(drawChartGrupoBarras);

        google.charts.setOnLoadCallback(drawChart);
        google.charts.setOnLoadCallback(drawChart2);
        google.charts.setOnLoadCallback(drawChartGrupo);
        //google.charts.setOnLoadCallback(drawChartPeriodicidad);

        <%-- Gráfico 1 por saldos por principal--%>

        function drawChart() {

            var data = google.visualization.arrayToDataTable(<%=buildChart()%>);
            var options = {
                isStacked: true,
                title: 'Saldo Principal ',

                colors: ['#b71c1c', '#388e3c', '#424242', '#ffff00', '#039be5', '#dd2c00'],
                chartArea: {
                    right: 0, // set this to adjust the legend width

                    left: 0, // set this to adjust the left margin

                },
                legend: {
                    position: 'right',

                },
                pieSliceText: 'percent',
                textStyle: {
                    fontSize: 10 // or the number you want
                },
            };
            document.getElementById("<%=test2.ClientID%>").style.display = 'none';

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var rangoMora = data.getValue(selectedItem.row, 0);
                    window.open("TablaDetalle.aspx?rangoMora=" + rangoMora + "&title=Distribución de cartera - Rango de Morosidad" + "&branches=" + "<%=getBranchesDistribution()%>" + "&date=" + document.getElementById("<%=txtDate.ClientID%>").value + "&officers=" + "<%=getOfficers()%>", '_blank');
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);

            chart.draw(data, options);
        }
        <%-- End -- Gráfico 1 por saldos de principal --%>

            <%-- Gráfico por clientes --%>
        function drawChart2() {

            var data = google.visualization.arrayToDataTable(<%=buildChartCliente()%>);

            var options = {
                pieHole: 0.4,
                title: ' Clientes ',
                legend: 'right',
                colors: ['#b71c1c', '#388e3c', '#424242', '#ffff00', '#039be5', '#dd2c00'],
                chartArea: {
                    right: 0, // set this to adjust the legend width   
                    left: 0, // set this to adjust the left margin

                },
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechartClient'));

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var rangoMora = data.getValue(selectedItem.row, 0);
                    window.open("TablaDetalle.aspx?rangoMora=" + rangoMora + "&title=Distribución de cartera - Rango de Morosidad" + "&branches=" + "<%=getBranchesDistribution()%>" + "&date=" + document.getElementById("<%=txtDate.ClientID%>").value + "&officers=<%=getOfficers()%>", '_blank');
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);
            chart.draw(data, options);
        }
        <%-- End -- Gráfico clientes --%>

     

        <%-- Gráfico por cartera por grupo --%>
        function drawChartGrupo() {

            var data = google.visualization.arrayToDataTable(<%=buildChartSaldoGrupo()%>);

            var options = {
                pieHole: 0.4,
                title: ' Distribución de Cartera Grupos - Clientes',
                legend: 'right',

                chartArea: {
                    right: 20, // set this to adjust the legend width
                    left: 20, // set this to adjust the left margin
                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechartSaldoGrupo'));

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var grupo = data.getValue(selectedItem.row, 0);
                    window.open("TablaDetalle.aspx?group=" + grupo + "&title=Distribución de cartera - Rango de Morosidad" + "&branches=" + "<%=getBranchesDistribution()%>" + "&date=" + document.getElementById("<%=txtDate.ClientID%>").value + "&officers=<%=getOfficers()%>", '_blank');
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);

            chart.draw(data, options);
        }

        <%-- END--- Gráfico por cartera por grupo --%>

         <%-- Gráfico por grupo por pendiente y principal --BARRAS--%>
        function drawChartGrupoBarras() {
            var data = google.visualization.arrayToDataTable(<%=buildChartSaldoPeriodicidadColumns()%>);

            var options = {
                title: 'Distribución de Cartera Grupos',
                chartArea: {
                    right: 10, // set this to adjust the legend width

                    left: 10, // set this to adjust the left margin
                },
                legend: {
                    position: 'right',

                },
                pieSliceText: 'percent',
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechartSaldoGrupoBarras'));

            google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var grupo = data.getValue(selectedItem.row, 0);

                    window.open("TablaDetalle.aspx?group=" + grupo + "&title=Distribución de cartera- Grupos" + "&branches=" + "<%=getBranchesDistribution()%>" + "&date=" + document.getElementById("<%=txtDate.ClientID%>").value + "&officers=<%=getOfficers()%>", '_blank');
                }
            }
            chart.draw(data, options);
        }
        <%-- END --- Gráfico por grupo por pendiente y principal --BARRAS--%>

        <%-- Gráfico por periodicidad--%>
        function drawChartPeriodicidad() {

            var data = google.visualization.arrayToDataTable(<%=buildChartSaldoPeriodicidad()%>);

            var options = {
                title: 'Distribución de cartera periodicidad- Semanal',
                legend: 'bottom',
                colors: ['#b71c1c', '#388e3c', '#424242', '#ffff00', '#039be5', '#dd2c00'],
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechartPeriodicidad'));

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var periodicidad = 'SEMANAL';
                    var rangoMora = data.getValue(selectedItem.row, 0);
                    window.open("TablaDetalle.aspx?periodicity=" + periodicidad + "&rangoMora=" + rangoMora + "&title=Distribución de cartera - Periodicidad" + "&branches=" + "<%=getBranchesDistribution()%>" + "&date=" + document.getElementById("<%=txtDate.ClientID%>").value + "&officers=<%=getOfficers()%>", '_blank');
                }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);

            chart.draw(data, options);
        }
        <%-- Gráfico por periodicidad-- Semanal --%>

        //Test apiladas
        function drawStacked() {
            var data = new google.visualization.arrayToDataTable(<%=buildChartSaldoPeriodicidadPivote()%>);

            var options = {
                title: 'Periodicidad de cartera',
                isStacked: true,
                hAxis: {
                },
                vAxis: {
                    title: 'Monto',
                    format: '#\'%\'',
                },
                colors: ['#388e3c', '#039be5', '#ffff00', '#b71c1c', '#dd2c00', '#424242'],
                chartArea: {
                    right: 80, // set this to adjust the legend width
                    left: 80, // set this to adjust the left margin
                },
                legend: 'bottom',
            };

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var col = chart.getSelection()[0]["column"];
                    var rangoMora = data.getColumnLabel(col);
                    var periodicity = data.getValue(selectedItem.row, 0);

                    window.open("TablaDetalle.aspx?periodicity=" + periodicity + "&rangoMora=" + rangoMora + "&title=Distribución de cartera - Periodicidad" + "&branches=<%=getBranchesDistribution()%>" + "&date=" + document.getElementById("<%=txtDate.ClientID%>").value + "&officers=<%=getOfficers()%>", '_blank');
                }
            }
            var chart = new google.visualization.ColumnChart(document.getElementById('piechartPeriodicidad'));

            google.visualization.events.addListener(chart, 'select', selectHandler);

            chart.draw(data, options);
        }
        //Fin testApiladas

        function openModalPeriodicidad() {
            $("#spnTitle").text("Hola");
            $("#spnMsg").text("Este es un msj");
            $('#modalPopUp').modal('show');
            return false;
        }
        function openModalDistribucion() {
            $("#spnTitle").text("Hola");
            $("#spnMsg").text("Este es un msj");

            $('#modalPopUpDistrucion').modal('show');
            return false;
        }
        function openModalGrupos() {
            $("#spnTitle").text("Hola");
            $("#spnMsg").text("Este es un msj");
            $('#modalPopUpGrupos').modal('show');
            return false;
        }
        function test() {
            if (document.getElementById("<%=ctnFilterId.ClientID%>").style.display === 'none') {
                document.getElementById("<%=ctnFilterId.ClientID%>").style.display = 'block'; 
                document.getElementById("<%=test.ClientID%>").style.display = 'block';
                document.getElementById("<%=test2.ClientID%>").style.display = 'none';

            }
            else {
                document.getElementById("<%=ctnFilterId.ClientID%>").style.display = 'none';
                document.getElementById("<%=test.ClientID%>").style.display = 'none';
                document.getElementById("<%=test2.ClientID%>").style.display = 'block';
            }
            return false;
        }
        function Validate() {
            var bool = false;
            var CHK = document.getElementById("<%=chkBranchesMother.ClientID%>");
            var checkbox = CHK.getElementsByTagName("input");
            var counter = 0;
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked) {
                    counter++;
                }
            }
            if (counter <= 0) {
                alert("¡Debe seleccionar al menos 1 sucursal!");
                bool = false;
            }
            else {
                bool = true;
            }
            return bool;
        }
    </script>
    <div class="container">

        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <div class="col-md-12" style="border-left: 2px">
                    <div class="box" style="line-height: 2">
                        <div class="box-header">
                            <i class="icon-book"></i>
                        </div>
                        <p>
                        </p>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" style="text-align:center">
                                        <h4 class="font-weight-bold text-primary">
                                            <asp:Label ID="titleForm" Text="Recuperación de Cartera Dirigida" runat="server" />
                                        </h4>
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <asp:HyperLink runat="server" onclick="return test();" ID="test"><i class="far fa-minus-square fa-2x"></i></asp:HyperLink>
                                                <asp:HyperLink runat="server" onclick="return test();" ID="test2"><i class="far fa-window-maximize fa-2x"></i></asp:HyperLink>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="card-bodyprinci" id="ctnFilterId" runat="server">
                                        <div class="col-md-3 ">
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:Label Text="Fecha:" runat="server" Font-Bold="true" />
                                                    <asp:TextBox runat="server" ID="txtDate" AutoPostBack="true" CssClass="inpdate" OnTextChanged="txtDate_TextChanged" />
                                                    <ajax:CalendarExtender ID="txtDateEndAjax" runat="server" TargetControlID="txtDate" Format="dd/MM/yyyy " />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="dropdown no-arrow">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label Text=" Sucursales:" runat="server" Font-Bold="true " Font-Size="16px" />
                                                        <a class="dropdown-toggle " href="#" style="margin-bottom: 10px" role="button" id="dropdownMenuLink123" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <i class="far fa-caret-square-down fa-2x" style="padding-top: 8px"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-out" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                                                            <div class="dropdown-header">Sucursal:</div>

                                                            <a class="dropdown-item  " href="#">
                                                                <asp:CheckBoxList ID="chkTotalMother" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkTotalMother_SelectedIndexChanged">
                                                                    <asp:ListItem Text="TODAS" Selected="True" />
                                                                </asp:CheckBoxList>
                                                                <asp:CheckBoxList ID="chkBranchesMother" runat="server" OnSelectedIndexChanged="chkBranchesMother_SelectedIndexChanged" AutoPostBack="true">
                                                                </asp:CheckBoxList>
                                                            </a>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="dropdown no-arrow">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label Text=" Usuarios:" runat="server" Font-Bold="true" Font-Size="16px" />
                                                        <asp:TextBox runat="server" CssClass="oficer inpdate" ID="txtOfficerSelected" Enabled="false" class="form-control" ClientIDMode="Static" />
                                                        <a class="dropdown-toggle " href="#" role="button" id="dropdownMenuLink123" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <i class="far fa-caret-square-down fa-2x" style="padding-top: 8px"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-out drop" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                                                            <div class="dropdown-header">Usuario:</div>
                                                            <a class="dropdown-item  " href="#">
                                                                <asp:RadioButtonList ID="chkOfficers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkOfficers_SelectedIndexChanged">
                                                                </asp:RadioButtonList>
                                                            </a>
                                                        </div>
                                                        <div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <span>
                                                <asp:Button runat="server" OnClientClick="return Validate();" ID="Button5" Text="Actualizar" OnClick="Button6_Click" CssClass="btn btn-primary" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- fin de primer row--%>
                    <%--     fin de titulos --%>
                    <%-- content card in row--%>
                    <div class="row">

                        <!-- card principal-->
                        <div class="col-md-3">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body" style="text-align: center">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Principal

                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <asp:Label ID="lblPrincipalId" Text="" runat="server" />

                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x "></i>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- card pendiente -->
                        <div class="col-md-3">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body" style="text-align: center">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pendiente</div>

                                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800 mrleftper">
                                                <asp:Label ID="lblPendientId" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-clipboard-list fa-2x "></i>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- card clientes-->
                        <div class="col-md-3">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body" style="text-align: center">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Clientes</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <asp:Label ID="lblClientId" Text="" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-users fa-2x "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card border-left-successret shadow h-100 py-2">
                                <div class="card-body" style="text-align: center">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Retención</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <asp:Label ID="lblRetention" Text="" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-user fa-2x "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p>
    </p>


    <p>
    </p>

    <!-- Pending Requests Card Example -->
    <%--cajas para primera tabla--%>

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Distribución de Cartera</h4>
                    <div class="dropdown no-arrow">
                        <div class="col-xl-12">
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="btnRefreshDistribution" Text="Actualizar" OnClick="btnRefreshDistribution_Click" CssClass="btn btn-primary" Visible="false" />
                            </div>
                        </div>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-out" style="margin-left: 200px" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                            <div class="dropdown-header">Opciones:</div>
                            <a class="dropdown-item " href="#">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkDistributionTotal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkDistributionTotal_SelectedIndexChanged">
                                            <asp:ListItem Text="TODAS" Selected="True" />
                                        </asp:CheckBoxList>
                                        <asp:CheckBoxList ID="chkListBranchesDistribucion" runat="server" OnSelectedIndexChanged="jaja_Click" AutoPostBack="true">
                                        </asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </div>
                        <asp:UpdatePanel runat="server" style="padding: 5px">
                            <ContentTemplate>

                                <asp:Button ID="Button3" OnClientClick="openModalPeriodicidad" Text="Ver detalles" runat="server" OnClick="btnVerTablaDistribucion_Click1 " CssClass=" btn btn-success" />

                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div id="piechart" style="height: 400px; max-width: 400px"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="piechartClient" style="width: 400px; height: 400px"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- fin de cajas para primera tabla--%>


    <%--inicio de cajas para tercer tabla--%>
    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Distribución por Grupos </h4>

                    <div class="dropdown no-arrow">
                        <div class="col-xl-12">
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="btnRefreshGroup" Text="Actualizar" CssClass="btn btn-primary" OnClick="btnRefreshGroup_Click" Visible="false" />
                            </div>

                        </div>




                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" style="margin-left: 200px" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                            <div class="dropdown-header">Opciones:</div>
                            <a class="dropdown-item" href="#">
                                <asp:UpdatePanel ID="UpdatePanel3" style="margin-left: 10px" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkGroupTotal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkGroupTotal_SelectedIndexChanged">
                                            <asp:ListItem Text="TODAS" Selected="True" />
                                        </asp:CheckBoxList>
                                        <asp:CheckBoxList ID="chkListBranchesGrupo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="chkListBranchesGrupo_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </div>
                        <asp:UpdatePanel runat="server" style="padding: 5px">
                            <ContentTemplate>
                                <asp:Button ID="Button4" Text="Ver detalles" runat="server" OnClick="btnAbrirModalPopupGrupo_Click" CssClass="btn btn-success" />

                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">

                        <div class="col-md-12">
                            <div id="piechartSaldoGrupoBarras" style="width: 400px; height: 400px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie Chart -->
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">

                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Distribución  por Grupos</h4>

                    <div class="dropdown no-arrow">
                        <div class="col-xl-12">
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="btnRefreshGroup2" Text="Actualizar" Visible="false" CssClass="btn btn-primary" OnClick="btnRefreshGroup2_Click" />
                            </div>

                        </div>

                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in " style="margin-left: 200px" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                            <div class="dropdown-header">Opciones:</div>
                            <a class="dropdown-item" href="#">
                                <asp:UpdatePanel ID="UpdatePanel4" style="margin-left: 10px" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkGroup2Total" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkGroup2Total_SelectedIndexChanged">
                                            <asp:ListItem Text="TODAS" Selected="True" />
                                        </asp:CheckBoxList>
                                        <asp:CheckBoxList ID="chkListBranchesGroup2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="chkListBranchesGroup2_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>

                        </div>
                        <asp:UpdatePanel runat="server" style="padding: 5px">
                            <ContentTemplate>
                                <asp:Button ID="Button1" Text="Ver detalles" runat="server" OnClick="btnAbrirModalPopupGrupo_Click" CssClass="btn btn-success" />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <div id="piechartSaldoGrupo" style="width: 400px; height: 400px"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%--        fin de cajas para tercera tabla-->--%>


    <%--inicio de cajas para segunda tabla--%>

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Distribución de Cartera- Periodicidad</h4>

                    <div class="dropdown no-arrow">
                        <div class="col-xl-12">
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="btnRefreshPeriodicity" Text="Actualizar" Visible="false" CssClass="btn btn-primary" OnClick="btnRefreshPeriodicity_Click" />
                            </div>

                        </div>

                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" style="margin-left: 200px" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                            <div class="dropdown-header">Opciones:</div>
                            <a class="dropdown-item" href="#">
                                <asp:UpdatePanel ID="UpdatePanel1" style="margin-left: 10px" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkPeriodicityTotal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkPeriodicityTotal_SelectedIndexChanged">
                                            <asp:ListItem Text="TODAS" Selected="True" />
                                        </asp:CheckBoxList>
                                        <asp:CheckBoxList ID="chkListBranchesPeriodicity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkListBranchesPeriodicity_SelectedIndexChanged"></asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </div>
                        <asp:UpdatePanel runat="server" style="padding: 5px">
                            <ContentTemplate>
                                <asp:Button ID="Button2" Text="Ver detalles" runat="server" OnClick="btnAbrirModalPeriodicidad_Click" CssClass="btn btn-success" />

                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">

                        <div class="col-md-12">
                            <div id="piechartPeriodicidad" style="width: 1110px; height: 400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--fin de cajas para segunda tabla--%>

    <%-- PopUp para periodicidad--%>
    <div id="modalPopUp" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-lg-12" style="width: 70%; align-content: center; text-align: center" role="document">
            <div class="modal-content">
                <div class="modal-header alert ">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="margin-left: 120px;">
                        <asp:Label runat="server" Text="Periodicidad" Font-Bold="true" Font-Size="X-Large" ID="spnTitle"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body modal-centertext ">
                    <div class="row">
                        <div class="col-md-12 modal-centertext">
                            <asp:GridView runat="server" ID="gvPeriodicidad" DataKeyNames="Rango_Morosidad,SEMANAL,QUINCENAL,MENSUAL,DIARIA,OTRO,SEMESTRAL" AutoGenerateColumns="false" CssClass="table" GridLines="None">
                                <Columns>
                                    <%--<asp:BoundField DataField="Rango_Morosidad" HeaderText="Rango_Morosidad" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Rango_Morosidad" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Rango_Morosidad" />

                                    <%--<asp:BoundField DataField="DIARIA" HeaderText="DIARIA" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="DIARIA" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}&periodicity=DIARIA" DataTextField="DIARIA" DataTextFormatString=" C${0:###,###,###.00}" />

                                    <%--<asp:BoundField DataField="SEMANAL" HeaderText="SEMANAL" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="SEMANAL" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}&periodicity=SEMANAL" DataTextField="SEMANAL" DataTextFormatString=" C${0:###,###,###.00}" />

                                    <%--<asp:BoundField DataField="QUINCENAL" HeaderText="QUINCENAL" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="QUINCENAL" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="QUINCENAL" DataTextFormatString=" C${0:###,###,###.00}" />

                                    <%--<asp:BoundField DataField="MENSUAL" HeaderText="MENSUAL" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="MENSUAL" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}&periodicity=MENSUAL" DataTextField="MENSUAL" DataTextFormatString=" C${0:###,###,###.00}" />
                                    <%--<asp:BoundField DataField="SEMESTRAL" HeaderText="SEMESTRAL" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="SEMESTRAL" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}&periodicity=SEMESTRAL" DataTextField="SEMESTRAL" DataTextFormatString=" C${0:###,###,###.00}" />

                                    <%--<asp:BoundField DataField="OTRO" HeaderText="OTRO" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="OTRO" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}&periodicity=OTRO" DataTextField="OTRO" DataTextFormatString=" C${0:###,###,###.00}" />

                                </Columns>

                            </asp:GridView>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--End PopUp para periodicidad End--%>
    <%-- PopUp para Distribución de cartera--%>
    <div id="modalPopUpDistrucion" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-lg-12" style="width: 70%; align-content: center; text-align: center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label runat="server" Text="Distribución de Cartera" Font-Bold="true" Font-Size="X-Large" ID="Label1"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body modal-centertext">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView runat="server" ID="gvTable" DataKeyNames="Rango_Morosidad,Principal,Clientes,Pendiente" AutoGenerateColumns="false" CssClass="table" GridLines="None">
                                <Columns>
                                    <%--<asp:BoundField DataField="Rango_Morosidad" HeaderText="Rango de Morosidad" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Rango de Morosidad" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Rango_Morosidad" />
                                    <%--<asp:BoundField DataField="Principal" HeaderText="Principal" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Principal" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Principal" DataTextFormatString=" C${0:###,###,###.00}" />
                                    <%--<asp:BoundField DataField="Clientes" HeaderText="Clientes" ShowHeader="True" Visible="True" />--%>

                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Pendiente" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Pendiente" DataTextFormatString=" C${0:###,###,###.00}" />
                                    <asp:HyperLinkField DataNavigateUrlFields="Rango_Morosidad" HeaderText="Clientes" DataNavigateUrlFormatString="TablaDetalle.aspx?rangoMora={0}" DataTextField="Clientes" />
                                    <%--<asp:BoundField DataField="Pendiente" HeaderText="Pendiente" ShowHeader="True" Visible="True" />--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <%-- END---- PopUp para Distribución de cartera --END --%>
    <%-- PopUp para Distribución de cartera--%>
    <div id="modalPopUpGrupos" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-lg-12" style="width: 60%; align-content: center; text-align: center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="margin-left: 100px">
                        <asp:Label runat="server" Text="Distribución por grupos" Font-Bold="true" Font-Size="X-Large" ID="Label2"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body modal-centertext">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView runat="server" ID="gvGrupo" DataKeyNames="Grupo,Clientes,Pendiente,Principal" AutoGenerateColumns="false" CssClass="table" GridLines="None">
                                <Columns>
                                    <%--<asp:BoundField DataField="Grupo" HeaderText="Grupo" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Grupo" HeaderText="Grupo" DataNavigateUrlFormatString="TablaDetalle.aspx?group={0}" DataTextField="Grupo" />
                                    <%--<asp:BoundField DataField="Principal" HeaderText="Principal" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Grupo" HeaderText="Principal" DataNavigateUrlFormatString="TablaDetalle.aspx?group={0}" DataTextField="Principal" DataTextFormatString=" C${0:###,###,###.00}" />

                                    <%--<asp:BoundField DataField="Pendiente" HeaderText="Pendiente" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Grupo" HeaderText="Pendiente" DataNavigateUrlFormatString="TablaDetalle.aspx?group={0}" DataTextField="Pendiente" DataTextFormatString=" C${0:###,###,###.00}" />

                                    <%--<asp:BoundField DataField="Clientes" HeaderText="Clientes" ShowHeader="True" Visible="True" />--%>
                                    <asp:HyperLinkField DataNavigateUrlFields="Grupo" HeaderText="Clientes" DataTextField="Clientes" />

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <%-- END---- PopUp para Distribución de cartera --END --%>
    <%-- END---- PopUp para Distribución de cartera --END --%>
</asp:Content>
