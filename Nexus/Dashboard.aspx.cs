﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
using Entidades;
using System.Data;
using System.Configuration;
using System.IO;
namespace Nexus
{
    public partial class Dashboard : System.Web.UI.Page
    {
        ResumenGerencialNegocio tb1 = new ResumenGerencialNegocio();
        NegocioActiveDirectory activeDirectory = new NegocioActiveDirectory();
        Utils utils = new Utils();
        public static string access = "";
        public static string branch = "";
        public List<string> listBranches = new List<string>();
        public static DataTable tableBranches = new DataTable();
        public static DataTable tableOfficers = new DataTable();
        public static DateTime dateMax = new DateTime();
        public ListItem listItemBranches = new ListItem();

        public static GridView dtPeriodicidadPivote = new GridView();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("inicio");
            }
            if (!IsPostBack)
            {
                access = (string)Session["access"];
                branch = (string)Session["branch"];
                titleForm.Text = "Recuperación de Cartera Dirigida- " + branch;
                fillBranches();
                setData();
            }
            //gvTableDatos.DataSource = tb1.getDataByName();
            //gvTableDatos.DataBind(); 

        }
        public void setDate()
        {
            txtDate.Text = Convert.ToDateTime(tb1.getMaxDate(tableBranches).Rows[0][0].ToString()).ToShortDateString();
            dateMax = Convert.ToDateTime(txtDate.Text);
        }
        public void fillBranches()
        {
            if (!access.Equals("Inmortal"))
            {
                tableBranches = tb1.getBranchByUser((string)Session["userName"]);
                listBranches.Add(tableBranches.Rows[0][0].ToString());
                foreach (DataRow row in tableBranches.Rows)
                {
                    string branchRow = row[0].ToString();

                    ListItem item = new ListItem();
                    item.Text = branchRow;
                    item.Selected = true;
                    item.Enabled = false;

                    chkBranchesMother.Items.Add(item);
                }
                foreach (ListItem item in chkTotalMother.Items)
                {
                    item.Enabled = false;
                }
            }
            else
            {
                tableBranches = tb1.getSucursales();
                foreach (DataRow row in tableBranches.Rows)
                {
                    string branchRow = row[0].ToString();
                    listBranches.Add(branchRow);

                    ListItem item = new ListItem();
                    item.Text = branchRow;
                    item.Selected = true;

                    chkBranchesMother.Items.Add(item);
                    chkListBranchesDistribucion.Items.Add(item);
                    chkListBranchesGrupo.Items.Add(item);
                    chkListBranchesPeriodicity.Items.Add(item);
                    chkListBranchesGroup2.Items.Add(item);
                }
            }
            setDate();
            fillOfficers();
        }
        public string getBranchesDistribution()
        {
            string data = "";
            if (access == "Mortal")
            {
                data = branch;
            }
            else
            {
                foreach (ListItem item in chkBranchesMother.Items)
                {
                    if (item.Selected)
                    {
                        data = data + item.Text + "|";
                    }
                }
            }
            return data;
        }
        public string getOfficers()
        {
            string data = "";
            string selection = chkOfficers.SelectedItem.Text;
            if (selection.Equals("TODOS"))
            {
                foreach (ListItem item in chkOfficers.Items)
                {
                    if (item.Text != "TODOS")
                    {
                        data = data + item.Text + "|";
                    }
                }
            }
            else
            {
                data = selection;
            }
            return data;
        }

        public string openModal()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            return "Ok";
        }
        public void setData()
        {
            gvTable.DataSource = tb1.getClient(tableBranches, tableOfficers, txtDate.Text);
            gvTable.DataBind();

            gvPeriodicidad.DataSource = tb1.getPeriodicidad(tableBranches, tableOfficers, txtDate.Text);
            gvPeriodicidad.DataBind();

            gvGrupo.DataSource = tb1.getSaldoGrupo(tableBranches, tableOfficers, txtDate.Text);
            gvGrupo.DataBind();

            dtPeriodicidadPivote.DataSource = tb1.getPeriodicidadPivote(tableBranches, tableOfficers, txtDate.Text);
            dtPeriodicidadPivote.DataBind();
        }
        public void recalculateDistribution(List<string> branchesToFind)
        {
            DataTable TbBranchesToFind = new DataTable();
            TbBranchesToFind.Columns.Add("StringItem", typeof(System.String));
            foreach (string branchToFind in branchesToFind)
            {
                TbBranchesToFind.Rows.Add(branchToFind);
            }
            foreach (DataRow item in tableOfficers.Rows)
            {
                string name = item[0].ToString();
            }
            foreach (DataRow item in TbBranchesToFind.Rows)
            {
                string name = item[0].ToString();
            }
            gvTable.DataSource = tb1.getClient(TbBranchesToFind, tableOfficers, txtDate.Text);
            gvTable.DataBind();
        }
        public void recalculateGroup(List<string> branchesToFind)
        {
            DataTable TbBranchesToFind = new DataTable();
            TbBranchesToFind.Columns.Add("StringItem", typeof(System.String));
            foreach (string branchToFind in branchesToFind)
            {
                TbBranchesToFind.Rows.Add(branchToFind);
            }
            gvGrupo.DataSource = tb1.getSaldoGrupo(TbBranchesToFind, tableOfficers, txtDate.Text);
            gvGrupo.DataBind();
        }
        public void recalculateGroup2(List<string> branchesToFind)
        {
            DataTable TbBranchesToFind = new DataTable();
            TbBranchesToFind.Columns.Add("StringItem", typeof(System.String));
            foreach (string branchToFind in branchesToFind)
            {
                TbBranchesToFind.Rows.Add(branchToFind);
            }
            gvGrupo.DataSource = tb1.getSaldoGrupo(TbBranchesToFind, tableOfficers, txtDate.Text);
            gvGrupo.DataBind();
        }
        public void recalculatePeriodicity(List<string> branchesToFind)
        {
            DataTable TbBranchesToFind = new DataTable();
            TbBranchesToFind.Columns.Add("StringItem", typeof(System.String));
            foreach (string branchToFind in branchesToFind)
            {
                TbBranchesToFind.Rows.Add(branchToFind);
            }

            gvPeriodicidad.DataSource = tb1.getPeriodicidad(TbBranchesToFind, tableOfficers, txtDate.Text);
            gvPeriodicidad.DataBind();

            dtPeriodicidadPivote.DataSource = tb1.getPeriodicidadPivote(TbBranchesToFind, tableOfficers, txtDate.Text);
            dtPeriodicidadPivote.DataBind();
        }

        public void updateTotalsForCards(string principal, string pendient, string client, string retention)
        {
            lblPrincipalId.Text = principal + "";
            lblPendientId.Text = pendient + "";
            lblClientId.Text = string.Format("{0:#,0}", Convert.ToInt32(client));
            lblRetention.Text = retention;
        }
        public string buildChart()
        {

            string data = "[['Rango Mora','Principal'],";
            int count = 0;
            int client = 0;
            double pendient = 0, principal = 0;

            foreach (GridViewRow row in gvTable.Rows)
            {
                count = count + 1;
                principal = principal + double.Parse(gvTable.DataKeys[count - 1].Values["Principal"].ToString());
                client = client + Convert.ToInt32(gvTable.DataKeys[count - 1].Values["Clientes"].ToString());
                pendient = pendient + double.Parse(gvTable.DataKeys[count - 1].Values["Pendiente"].ToString());

                data = data + "[";
                data = data + "'" + gvTable.DataKeys[count - 1].Values["Rango_Morosidad"].ToString() + "'," + (Convert.ToDecimal(gvTable.DataKeys[count - 1].Values["Principal"].ToString()) + "").Replace(",", ".");
                data = count != gvTable.Rows.Count ? data + "]," : data + "]";
            }
            updateTotalsForCards(utils.formatCoin(principal), utils.formatCoin(pendient), client + "", tableBranches.Rows.Count > 0 && tableOfficers.Rows.Count > 0 ? utils.formatDecimal(Convert.ToDecimal(tb1.getRetention(tableBranches, tableOfficers, txtDate.Text).Rows[0]["RETENCION"].ToString())) : "");
            return data = data + "]";
        }
        public string buildChartSaldoPeriodicidadColumns()
        {
            string data = "[['Grupo','Principal'],";
            int count = 0;

            int client = 0;
            double pendient = 0, principal = 0;
            foreach (GridViewRow row in gvGrupo.Rows)
            {
                count = count + 1;
                data = data + "[";
                data = data + "'" + gvGrupo.DataKeys[count - 1].Values["Grupo"].ToString() + "', " + (Convert.ToDecimal(gvGrupo.DataKeys[count - 1].Values["Principal"].ToString()) + "").Replace(",", ".");
                data = count != gvGrupo.Rows.Count ? data + "]," : data + "]";

                principal = principal + double.Parse(gvGrupo.DataKeys[count - 1].Values["Principal"].ToString());
                client = client + Convert.ToInt32(gvGrupo.DataKeys[count - 1].Values["Clientes"].ToString());
                pendient = pendient + double.Parse(gvGrupo.DataKeys[count - 1].Values["Pendiente"].ToString());

            }
            return data = data + "]";
        }
        public string buildChartCliente()
        {

            string data = "[['Rango Mora','Clientes'],";
            int count = 0;
            foreach (GridViewRow row in gvTable.Rows)
            {
                count = count + 1;
                data = data + "[";
                data = data + "'" + gvTable.DataKeys[count - 1].Values["Rango_Morosidad"].ToString() + "'," + gvTable.DataKeys[count - 1].Values["Clientes"].ToString();
                data = count != gvTable.Rows.Count ? data + "]," : data + "]";
            }
            return data = data + "]";
        }

        public string buildChartPendient()
        {

            string data = "[['Rango Mora','Pendiente'],";
            int count = 0;
            foreach (GridViewRow row in gvTable.Rows)
            {
                count = count + 1;
                data = data + "[";
                data = data + "'" + gvTable.DataKeys[count - 1].Values["Rango_Morosidad"].ToString() + "'," + (Convert.ToDecimal(gvTable.DataKeys[count - 1].Values["Pendiente"].ToString()) + "").Replace(",", ".");
                data = count != gvTable.Rows.Count ? data + "]," : data + "]";
            }
            return data = data + "]";
        }
        public string buildChartSaldoGrupo()
        {
            string data = "[['Grupo','Clientes'],";
            int count = 0;
            foreach (GridViewRow row in gvGrupo.Rows)
            {
                count = count + 1;
                data = data + "[";
                data = data + "'" + gvGrupo.DataKeys[count - 1].Values["Grupo"].ToString() + "'," + gvGrupo.DataKeys[count - 1].Values["Clientes"].ToString();
                data = count != gvGrupo.Rows.Count ? data + "]," : data + "]";
            }
            return data = data + "]";
        }

        public string buildChartPeriodicidadDetail()
        {
            string data = "[['Rango Mora','DIARIA','SEMANAL','QUINCENAL','MENSUAL','SEMESTRAL','OTRO'],";
            int count = 0;
            foreach (GridViewRow row in gvPeriodicidad.Rows)
            {
                count = count + 1;
                data = data + "[";
                data = data + "'" + gvPeriodicidad.DataKeys[count - 1].Values["Rango_Morosidad"].ToString() + "'," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["SEMANAL"].ToString()) + "").Replace(",", ".") + "," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["QUINCENAL"].ToString()) + "").Replace(",", ".") + "," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["MENSUAL"].ToString()) + "").Replace(",", ".") + "," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["DIARIA"].ToString()) + "").Replace(",", ".") + "," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["OTRO"].ToString()) + "").Replace(",", ".") + "," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["SEMESTRAL"].ToString()) + "").Replace(",", ".");
                data = count != gvPeriodicidad.Rows.Count ? data + "]," : data + "]";
            }
            return data = data + "]";
        }
        public string buildChartSaldoPeriodicidad()
        {
            string data = "[['Rango Mora','Diaria','Semanal','Quincenal','Mensual','Semestral','Otro'],";
            int count = 0;
            foreach (GridViewRow row in gvPeriodicidad.Rows)
            {
                count = count + 1;
                data = data + "[";
                data = data + "'" + gvPeriodicidad.DataKeys[count - 1].Values["Rango_Morosidad"].ToString() + "'," + (Convert.ToDecimal(gvPeriodicidad.DataKeys[count - 1].Values["SEMANAL"].ToString()) + "").Replace(",", ".");
                data = count != gvPeriodicidad.Rows.Count ? data + "]," : data + "]";
            }
            return data = data + "]";
        }
        public string buildChartSaldoPeriodicidadPivote()
        {
            string data = "[['Periodicidad','Credito_Al_Dia','Mora_Temprana','Mora_Avanzada','Alta_Morosidad','Perdida_Potencial','Credito_Vencido'],";
            int count = 0;

            decimal total = 0;
            foreach (GridViewRow row in dtPeriodicidadPivote.Rows)
            {
                count = count + 1;
                total = Decimal.Parse(row.Cells[1].Text) + Decimal.Parse(row.Cells[2].Text) + Decimal.Parse(row.Cells[3].Text) + Decimal.Parse(row.Cells[4].Text) + Decimal.Parse(row.Cells[5].Text) + Decimal.Parse(row.Cells[6].Text);
                data = data + "[";

                data = data + "'" + row.Cells[0].Text + "'," + (Convert.ToDecimal(row.Cells[1].Text) * 100 / total + "").Replace(",", ".") + "," + (Convert.ToDecimal(row.Cells[2].Text) * 100 / total + "").Replace(",", ".") + "," + (Convert.ToDecimal(row.Cells[3].Text) * 100 / total + "").Replace(",", ".") + "," + (Convert.ToDecimal(row.Cells[4].Text) * 100 / total + "").Replace(",", ".") + "," + (Convert.ToDecimal(row.Cells[5].Text) * 100 / total + "").Replace(",", ".") + "," + (Convert.ToDecimal(row.Cells[6].Text) * 100 / total + "").Replace(",", ".");

                data = count != gvPeriodicidad.Rows.Count ? data + "]," : data + "]";
            }

            return data = data + "]";
        }

        protected void btnVerTablaDistribucion_Click1(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalDistribucion();", true);
        }

        protected void btnAbrirModalPeriodicidad_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalPeriodicidad();", true);
        }

        protected void btnAbrirModalPopupGrupo_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalGrupos();", true);

        }

        protected void chkListBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in chkListBranchesDistribucion.Items)
            {
                if (item.Selected)
                {

                }
            }
        }

        protected void jaja_Click(object sender, EventArgs e)
        {
            bool selected = true;
            foreach (ListItem item in chkListBranchesDistribucion.Items)
            {
                if (!item.Selected)
                {
                    selected = false;
                    break;
                }
            }
            foreach (ListItem item in chkDistributionTotal.Items)
            {
                item.Selected = selected;
            }
        }

        protected void chkListBranchesGroup2_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool selected = true;
            foreach (ListItem item in chkListBranchesGroup2.Items)
            {
                if (!item.Selected)
                {
                    selected = false;
                    break;
                }
            }
            foreach (ListItem item in chkGroup2Total.Items)
            {
                item.Selected = selected;
            }
        }

        protected void chkDistributionTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in chkDistributionTotal.Items)
            {
                if (item.Selected)
                {
                    setValues(chkListBranchesDistribucion, true);
                }
                else
                {
                    setValues(chkListBranchesDistribucion, false);
                }
            }
        }
        private void setValues(CheckBoxList listBox, bool value)
        {
            foreach (ListItem item in listBox.Items)
            {
                item.Selected = value;
            }
        }
        protected void btnRefreshDistribution_Click(object sender, EventArgs e)
        {
            List<string> listToFind = new List<string>();
            foreach (ListItem item in chkListBranchesDistribucion.Items)
            {
                if (item.Selected)
                {
                    listToFind.Add(item.Text);
                }
            }
            recalculateDistribution(listToFind);
        }

        protected void chkListBranchesGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool selected = true;
            foreach (ListItem item in chkListBranchesGrupo.Items)
            {
                if (!item.Selected)
                {
                    selected = false;
                    break;
                }
            }
            foreach (ListItem item in chkGroupTotal.Items)
            {
                item.Selected = selected;
            }
        }

        protected void chkListBranchesPeriodicity_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool selected = true;
            foreach (ListItem item in chkListBranchesGrupo.Items)
            {
                if (!item.Selected)
                {
                    selected = false;
                    break;
                }
            }
            foreach (ListItem item in chkGroupTotal.Items)
            {
                item.Selected = selected;
            }
        }

        protected void btnRefreshGroup_Click(object sender, EventArgs e)
        {
            List<string> listToFind = new List<string>();
            foreach (ListItem item in chkListBranchesGrupo.Items)
            {
                if (item.Selected)
                {
                    listToFind.Add(item.Text);
                }
            }
            recalculateGroup(listToFind);
        }

        protected void btnRefreshGroup2_Click(object sender, EventArgs e)
        {
            List<string> listToFind = new List<string>();
            foreach (ListItem item in chkListBranchesGroup2.Items)
            {
                if (item.Selected)
                {
                    listToFind.Add(item.Text);
                }
            }
            recalculateGroup2(listToFind);
        }

        protected void btnRefreshPeriodicity_Click(object sender, EventArgs e)
        {
            List<string> listToFind = new List<string>();
            foreach (ListItem item in chkListBranchesPeriodicity.Items)
            {
                if (item.Selected)
                {
                    listToFind.Add(item.Text);
                }
            }
            recalculatePeriodicity(listToFind);
        }

        protected void chkGroup2Total_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in chkGroup2Total.Items)
            {
                if (item.Selected)
                {
                    setValues(chkListBranchesGroup2, true);
                }
                else
                {
                    setValues(chkListBranchesGroup2, false);
                }
            }
        }

        protected void chkGroupTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in chkGroupTotal.Items)
            {
                if (item.Selected)
                {
                    setValues(chkListBranchesGrupo, true);
                }
                else
                {
                    setValues(chkListBranchesGrupo, false);
                }
            }
        }
        protected void chkPeriodicityTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in chkPeriodicityTotal.Items)
            {
                if (item.Selected)
                {
                    setValues(chkListBranchesPeriodicity, true);
                }
                else
                {
                    setValues(chkListBranchesPeriodicity, false);
                }
            }
        }

        protected void x(object sender, EventArgs e)
        {
            bool selected = true;
            foreach (ListItem item in chkListBranchesPeriodicity.Items)
            {
                if (!item.Selected)
                {
                    selected = false;
                    break;
                }
            }
            foreach (ListItem item in chkPeriodicityTotal.Items)
            {
                item.Selected = selected;
            }
        }
        protected void chkBranchesMother_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool selected = true;
            tableBranches = new DataTable();
            tableBranches.Columns.Add("StringItem", typeof(System.String));
            foreach (ListItem item in chkBranchesMother.Items)
            {
                if (item.Selected)
                {
                    tableBranches.Rows.Add(item.Text);
                }
                else
                {
                    selected = false;
                }
            }
            foreach (ListItem item in chkTotalMother.Items)
            {
                item.Selected = selected;
            }
            fillOfficers();
        }
        protected void chkTotalMother_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListItem item in chkTotalMother.Items)
            {
                if (item.Selected)
                {
                    setValues(chkBranchesMother, true);
                }
                else
                {
                    setValues(chkBranchesMother, false);
                }
            }
            tableBranches = new DataTable();
            tableBranches.Columns.Add("StringItem", typeof(System.String));
            foreach (ListItem item in chkBranchesMother.Items)
            {
                if (item.Selected)
                {
                    tableBranches.Rows.Add(item.Text);
                }
                else
                {
                    break;
                }
            }
            fillOfficers();
        }

        protected void chkBranchesMother_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            List<string> listToFind = new List<string>();
            tableBranches = new DataTable();
            tableBranches.Columns.Add("StringItem", typeof(System.String));

            tableOfficers = new DataTable();
            tableOfficers.Columns.Add("StringItem", typeof(System.String));

            foreach (ListItem item in chkBranchesMother.Items)
            {
                if (item.Selected)
                {
                    tableBranches.Rows.Add(item.Text);
                    listToFind.Add(item.Text);
                }
            }
            if (chkOfficers.Items.FindByText("TODOS").Selected)
            {
                foreach (ListItem item in chkOfficers.Items)
                {
                    tableOfficers.Rows.Add(item.Text);
                }
            }
            else
            {
                tableOfficers.Rows.Add(chkOfficers.SelectedItem.Text);
            }
            if (tableBranches.Rows.Count > 0 && tableOfficers.Rows.Count > 0)
            {
                recalculateDistribution(listToFind);
                recalculateGroup(listToFind);
                recalculateGroup2(listToFind);
                recalculatePeriodicity(listToFind);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('¡Debe seleccionar al menos una sucursal y un usuario!');", false);
            }

        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            try
            {
                date = Convert.ToDateTime(txtDate.Text);
                if (date >= DateTime.Now || date >= dateMax)
                {
                    setDate();
                }
            }
            catch (Exception)
            {
                setDate();
            }
            fillOfficers();
        }
        private void fillOfficers()
        {
            tableOfficers = tableBranches.Rows.Count > 0 ? tb1.getRecollectionOfficer(tableBranches, txtDate.Text) : new DataTable();
            List<ListItem> toRemove = new List<ListItem>();

            foreach (ListItem item in chkOfficers.Items)
            {
                toRemove.Add(item);
            }
            foreach (ListItem item in toRemove)
            {
                chkOfficers.Items.Remove(item);
            }

            ListItem allItem = new ListItem();
            allItem.Selected = true;
            allItem.Text = "TODOS";
            chkOfficers.Items.Add(allItem);

            foreach (DataRow row in tableOfficers.Rows)
            {
                ListItem item = new ListItem();
                item.Text = row[0].ToString();
                chkOfficers.Items.Add(item);
            }
            txtOfficerSelected.Text = chkOfficers.SelectedItem.Text;
        }

        protected void chkOfficers_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtOfficerSelected.Text = chkOfficers.SelectedItem.Text;
        }
      

    }
}