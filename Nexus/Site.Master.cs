﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nexus
{
    public partial class SiteMaster : MasterPage
    {
        

        

        protected void Page_Load(object sender, EventArgs e)
        {
            lblusuario.Text = (string)Session["userName"];

            
        }

            

    public void lnksalir_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            ViewState.Clear();
            FormsAuthentication.SignOut();
            Session.Remove("access");
            Session.Remove("brach");
            Session.Remove("userName");
            Response.Redirect("~/inicio.aspx");
        }

    }

}