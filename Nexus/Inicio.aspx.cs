﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Nexus.Models;
using Negocio;
using System.Configuration;
using System.Collections.Generic;

namespace Nexus
{
    public partial class Inicio : System.Web.UI.Page
    {
        NegocioActiveDirectory activeDirectory = new NegocioActiveDirectory();
        ResumenGerencialNegocio rGerencialNegocio = new ResumenGerencialNegocio();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUserName.Text) && !string.IsNullOrEmpty(txtPassword.Text))
            {
                try
                {
                    //if (activeDirectory.usuarioValido(txtUserName.Text, txtPassword.Text))
                    //{
                        Session["userName"] = txtUserName.Text;
                        string grupo1 = ConfigurationManager.AppSettings["Grupo1"].ToString();
                        string grupo2 = ConfigurationManager.AppSettings["Grupo2"].ToString();
                        List<String> grupos = new List<String>();
                        //grupos = activeDirectory.mostrarGruposParaUsuario(txtUserName.Text);
                        //  Linea para administrador.
                        grupos.Add("NxAllowedUsers");
                        //----------------------------------------------------------------------

                        // Linea para mortales (Dejar comentada 1 de las 2).
                        grupos.Add("NxAdministrators");
                        // ----------------------------------------------------------------------
                        if (grupos.Contains(grupo1))
                        {
                            Session["access"] = "Inmortal";
                            Session["branch"] = "Administrador";
                            Session["username"] = txtUserName.Text.Trim().ToLower();
                            Response.Redirect("Dashboard.aspx");
                        }
                        else
                        {
                            if (grupos.Contains(grupo2))
                            {
                                Session["access"] = "Mortal";
                                Session["branch"] = rGerencialNegocio.getBranchByUser(txtUserName.Text).Rows[0][0].ToString();
                                Response.Redirect("Dashboard.aspx");
                            }
                        }
                    //}
                    //else
                    //{
                    //    lblmsg.Text = "Error autenticando... ";
                    //    lblmsg.CssClass = "alert alert-warning";
                    //    lblmsg.ForeColor = System.Drawing.Color.Red;
                    //}
                }
               catch (Exception ex)
                {
                    lblmsg.Text = "Error autenticando... " + ex.Message;
                    lblmsg.CssClass = "alert alert-warning";
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                lblmsg.Text = "¡Debe completar los campos!";
                lblmsg.CssClass = "alert alert-warning";
                lblmsg.ForeColor = System.Drawing.Color.Red;
            }


        }
    }
}