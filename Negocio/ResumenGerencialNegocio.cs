﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Datos;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Negocio
{
    public class ResumenGerencialNegocio
    {
        public ResumenGerencialNegocio() { }
        ResumenGerencial rGerencial = new ResumenGerencial();

        public DataTable getClient(DataTable branches,DataTable officers,string date)
        {
            return rGerencial.pr_getDataCli(branches, officers, date);
        }

        public DataTable getPeriodicidad(DataTable dt, DataTable officers,string date)
        {
            return rGerencial.pr_getPeriodicidad(dt, officers, date);
        }
        public DataTable getPeriodicidadPivote(DataTable dt, DataTable officers,string date)
        {
            return rGerencial.pr_getPeriodicidadPivote(dt, officers,date);
        }

        public DataTable getSaldoGrupo(DataTable branches,DataTable officers,string date)
        {
            return rGerencial.pr_getSaldoGrupo(branches, officers, date);
        }
        public DataTable getSucursales()
        {
            return rGerencial.pr_getSucursales();
        }
        public string getAllBranchesString()
        {
            string data = "";
            int cont = 0;
            DataTable dtBranches= rGerencial.pr_getSucursales();
            string item = "";
            foreach (DataRow row in dtBranches.Rows)
            {
                cont++;
                item = row[0].ToString();
                data = cont != dtBranches.Rows.Count ? data + item + "|" : data + item;
            }
            return data;
        }

        public DataTable getDataByName(DataTable branches,DataTable officers, string rangoMora, string group, string periodicity,string date)
        {
            return rGerencial.getDataByName(branches, officers, rangoMora, group, periodicity, date);
        }
        public DataTable getBranchByUser(string userName)
        {
            return rGerencial.getBranchByUser(userName);
        }
        public DataTable getRetention(DataTable branches,DataTable officers ,string date)
        {
            return rGerencial.getRetention(branches, officers, date);
        }
        public DataTable getRecollectionOfficer(DataTable branches, string date)
        {
            return rGerencial.getRecollectionOfficer(branches, date);
        }
        public DataTable getMaxDate(DataTable branches)
        {
            return rGerencial.getMaxDate(branches);
        }


    }
}
