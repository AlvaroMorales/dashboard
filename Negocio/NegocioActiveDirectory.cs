﻿
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Data;
using Datos;

namespace Negocio
{
    public class NegocioActiveDirectory
    {


        LdapAuthentication ldap = new LdapAuthentication();

        /// <summary>
        /// Valida si el usuario y la contraseña introducidos son correctos
        /// </summary>
        /// <param name="usuario">El nombre de usuario en Active Directory</param>
        /// <param name="password">Pasword del usuario en Active Directory</param>
        /// <returns>Verdadero o falso</returns>
        public bool usuarioValido(String usuario, String password)
        {
            return ldap.IsAuthenticated("crediexpress", usuario, password);
        }

        /// <summary>
        /// Muestra los grupos a los que pertenece el usuario en el Active Directory
        /// </summary>
        /// <param name="usuario">El nombre de usuario en Active Directory</param>
        /// <returns>
        /// Lista de grupos a los que pertenece el usuario
        /// </returns>
        public List<String> mostrarGruposParaUsuario(String usuario)
        {
            List<String> grupos = new List<String>();
            foreach (GroupPrincipal grupo in ldap.getGroupsForUser(usuario))
            {
                grupos.Add(grupo.Name);
            }
            return grupos;
        }

        /// <summary>
        /// Muestra los grupos a los que pertenece el usuario en el Active Directory 
        /// </summary>
        /// <param name="usuario">El nombre de usuario en Active Directory</param>
        /// <returns>Cadena de texto concatenada de los grupos a los que pertenece el usuario</returns>
        public String mostrarGruposParaUsuarioString(String usuario)
        {
            String strGrupo = "";
            foreach (GroupPrincipal grupo in ldap.getGroupsForUser(usuario))
            {
                strGrupo += "/" + grupo.Name;
            }
            return strGrupo;
        }

        /// <summary>
        /// Muestra el nombre de la persona asociada al username
        /// </summary>
        /// <param name="usuario">El nombre de usuario en Active Directory</param>
        /// <returns>cadena de texto con el nombre de la persona</returns>
        public String mostrarNombreUsuario(String usuario)
        {
            return ldap.GetFullName(usuario);
        }

        public String obtenerCorreoUsuario(String userName)
        {
            return ldap.getEmail(userName);
        }

        public string mostrarArea(string userName)
        {
            return ldap.getOffice(userName);
        }

        public void mostrarPropiedadDistinct(String propiedad)
        {
            ldap.getDistinctProperty(propiedad);
        }


    }
}