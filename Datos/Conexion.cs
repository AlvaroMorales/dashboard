﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Conexion
    {
        static string cnn = ConfigurationManager.ConnectionStrings["nexusConexion"].ConnectionString;

        public Conexion() { }

        public string getConexion()
        {
            if (object.ReferenceEquals(cnn, string.Empty))
            {
                return string.Empty;
            }
            else
            {
                return cnn;
            }
        }
    }
}
