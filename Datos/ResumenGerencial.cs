﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class ResumenGerencial
    {

        SqlConnection cnx;
        Conexion miConex = new Conexion();
        SqlCommand cmd = new SqlCommand();

        public ResumenGerencial()
        {
            cnx = new SqlConnection(miConex.getConexion());
        }

        public DataTable pr_getDataCli(DataTable branches, DataTable officers, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_RANGO_MORA";

                cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                cmd.Parameters["@date"].Value = date;

                cmd.Parameters.Add(new SqlParameter("@Combo", SqlDbType.Structured));
                cmd.Parameters["@Combo"].Value = branches;

                cmd.Parameters.Add(new SqlParameter("@officers", SqlDbType.Structured));
                cmd.Parameters["@officers"].Value = officers;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable pr_getPeriodicidad(DataTable branches, DataTable officers, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_Saldo_Periodicidad";

                SqlParameter sp = new SqlParameter();
                sp.SqlDbType = SqlDbType.Structured;
                sp.Value = branches;
                sp.ParameterName = "@Combo";

                cmd.Parameters.Add(new SqlParameter("@officers", SqlDbType.Structured));
                cmd.Parameters["@officers"].Value = officers;

                cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                cmd.Parameters["@date"].Value = date;

                cmd.Parameters.Add(sp);

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable pr_getPeriodicidadPivote(DataTable branches, DataTable officers, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_Saldo_Periodicidad_Pivote";

                SqlParameter sp = new SqlParameter();
                sp.SqlDbType = SqlDbType.Structured;
                sp.Value = branches;
                sp.ParameterName = "@Combo";

                cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                cmd.Parameters["@date"].Value = date;

                cmd.Parameters.Add(new SqlParameter("@officers", SqlDbType.Structured));
                cmd.Parameters["@officers"].Value = officers;

                cmd.Parameters.Add(sp);

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable pr_getSaldoGrupo(DataTable branches, DataTable officers, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_Saldo_Grupo";

                SqlParameter sp = new SqlParameter();
                sp.SqlDbType = SqlDbType.Structured;
                sp.Value = branches;
                sp.ParameterName = "@Combo";

                cmd.Parameters.Add(new SqlParameter("@officers", SqlDbType.Structured));
                cmd.Parameters["@officers"].Value = officers;

                cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                cmd.Parameters["@date"].Value = date;

                cmd.Parameters.Add(sp);

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable pr_getSucursales()
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_GET_ALL_BRANCHES";

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }


        public DataTable getDataByName(DataTable branches, DataTable officers, string rangoMora, string group, string periodicity, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_Data_By_Name";

                cmd.Parameters.Add(new SqlParameter("@Combo", SqlDbType.Structured));
                cmd.Parameters["@Combo"].Value = branches;

                cmd.Parameters.Add(new SqlParameter("@officers", SqlDbType.Structured));
                cmd.Parameters["@officers"].Value = officers;

                cmd.Parameters.Add(new SqlParameter("@rangoMora", SqlDbType.VarChar, 50));
                cmd.Parameters["@rangoMora"].Value = rangoMora;

                cmd.Parameters.Add(new SqlParameter("@group", SqlDbType.VarChar, 50));
                cmd.Parameters["@group"].Value = group;

                cmd.Parameters.Add(new SqlParameter("@periodicity", SqlDbType.VarChar, 50));
                cmd.Parameters["@periodicity"].Value = periodicity;

                cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                cmd.Parameters["@date"].Value = date;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable getBranchByUser(string userName)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_GET_BRANCH_BY_USER";


                cmd.Parameters.Add(new SqlParameter("@userName", SqlDbType.VarChar, 20));
                cmd.Parameters["@userName"].Value = userName;


                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            string n = dts.Tables["data"].Rows[0][0].ToString();
            return (dts.Tables["data"]);
        }
        public DataTable getRetention(DataTable branches, DataTable officers, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPE_GET_RETENCION";

                cmd.Parameters.Add(new SqlParameter("@officers", SqlDbType.Structured));
                cmd.Parameters["@officers"].Value = officers;

                cmd.Parameters.Add(new SqlParameter("@Combo", SqlDbType.Structured));
                cmd.Parameters["@Combo"].Value = branches;

                cmd.Parameters.Add(new SqlParameter("@FECHA_HOY", SqlDbType.Date));
                cmd.Parameters["@FECHA_HOY"].Value = date;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable getRecollectionOfficer(DataTable branches, string date)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_Recollection_Officer";

                cmd.Parameters.Add(new SqlParameter("@Combo", SqlDbType.Structured));
                cmd.Parameters["@Combo"].Value = branches;

                cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.Date));
                cmd.Parameters["@date"].Value = date;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
        public DataTable getMaxDate(DataTable branches)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USPCE_Get_Max_Date";

                cmd.Parameters.Add(new SqlParameter("@Combo", SqlDbType.Structured));
                cmd.Parameters["@Combo"].Value = branches;

                string name = branches.Rows[0][0].ToString();

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "data");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
                //string menssaje = ex.Message;
                //mcentidad2.queryresp = menssaje;
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["data"]);
        }
    }
}

